package za.co.sb.devserver.clients.devops;

import javax.enterprise.context.Dependent;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

/**
 * Rest Client interface to the Andon Cord DevOps BOT
 * @author steven
 */
@RegisterRestClient
@Dependent
@Path("/andoncord")
public interface DevOpsAndonCord {
    
    @GET
    @Path("/status")
    Status getStatus();
}
