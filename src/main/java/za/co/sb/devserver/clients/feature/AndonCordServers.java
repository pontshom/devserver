/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.sb.devserver.clients.feature;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

/**
 *
 * @author steven
 */
public class AndonCordServers {
    @Schema(description = "An array of servers to notify", example = "{\n"
                        + "  \"andonCordServerDetails\" : [ {\n"
                        + "    \"notificationURL\" : \"http://localhost:8104/\",\n"
                        + "    \"serverName\" : \"Valid Service\"\n"
                        + "  }, {\n"
                        + "    \"notificationURL\" : \"http://localhost:8104/\",\n"
                        + "    \"serverName\" : \"DelServer\"\n"
                        + "  } ]\n"
                        + "}")
    private AndonCordServerDetails[] andonCordServerDetails;

    public AndonCordServerDetails[] getAndonCordServerDetails() {
        return andonCordServerDetails;
    }

    public void setAndonCordServerDetails(AndonCordServerDetails[] andonCordServerDetails) {
        this.andonCordServerDetails = andonCordServerDetails;
    }
        
}
