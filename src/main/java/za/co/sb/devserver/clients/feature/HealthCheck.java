package za.co.sb.devserver.clients.feature;

import java.util.Map;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

/**
 * A specific health checks details and result
 *
 * @author steven
 */
public class HealthCheck {

    @Schema(description = "The name of the check down. E.g. memory-check")
    String name;
    
    @Schema(description = "The state reported by the check. UP or DOWN")
    String state;
    
    @Schema(description = "Any number of data elements for the check. E.g. free memory. Key value pair")
    Map<String, String> data;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Map<String, String> getData() {
        return data;
    }
    public void setData(Map<String, String> data) {
        this.data = data;
    }
}
