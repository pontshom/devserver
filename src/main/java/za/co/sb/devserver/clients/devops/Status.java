package za.co.sb.devserver.clients.devops;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

/**
 *
 * @author steven
 */
public class Status {
    
    @Schema(description = "true if the andon cord is active, false if not", example = "true")
    Boolean andon_cord_status;
        
    @Schema(description = "An Array of Strings, each representing the broken builds and branch name E.g. [\"payments - development\", \"liqubase - master\"", name = "broken_builds", example = "Use the description example here")
    String[] broken_builds;
    
    public Boolean getAndon_cord_status() {
        return andon_cord_status;
    }

    public void setAndon_cord_status(Boolean status) {
        this.andon_cord_status = status;
    }

    public String[] getBroken_builds() {
        return broken_builds;
    }

    public void setBroken_builds(String[] broken_builds) {
        this.broken_builds = broken_builds;
    }
  
}
