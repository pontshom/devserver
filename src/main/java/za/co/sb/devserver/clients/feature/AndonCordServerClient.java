/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.sb.devserver.clients.feature;

import javax.enterprise.context.Dependent;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import za.co.sb.devserver.clients.devops.Status;

/**
 *
 * @author steven
 */
@RegisterRestClient
@Dependent
public interface AndonCordServerClient {

    @PUT
    @Path("/services")    
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(description = "This is the register method that will be called on your service")
    public Response register(AndonCordServers andonCordServers);
    
    @PUT
    @Path("/andoncord")    
    @Consumes(MediaType.APPLICATION_JSON)    
    @Operation(description = "This is the status method that will be called on your service")
    public Response status(Status status);

}
