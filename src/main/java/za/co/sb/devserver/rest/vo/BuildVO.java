package za.co.sb.devserver.rest.vo;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Objects;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

/**
 *
 * @author pontsho
 */
public class BuildVO {

    @Schema(description = "name of the service", example = "true")
    private String serviceName;
    
    @Schema(description = "true if the andon cord is active, false if not", example = "true")
    private Boolean broken;
    
    @Schema(description = "Date and time the andon cord was bacame active", example = "2020-02-01 06:00")
    private LocalDateTime createdAt;
    
    @Schema(description = "Date and time the andon cord was fixed", example = "2020-02-01 09:00")
    private LocalDateTime fixedAt;

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    } 
   
    public Boolean getBroken() {
        return broken;
    }

    public void setBroken(Boolean broken) {
        this.broken = broken;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getFixedAt() {
        return fixedAt;
    }

    public void setFixedAt(LocalDateTime fixedAt) {
        this.fixedAt = fixedAt;
    }
    
    /**
    *
    * using hours still, anything less than an hour will be 0 and if the cord is not fixed yet will make it -1 
    */
    public long getRepairTime() {
        //if fixedAt is null this will remain and it will mean the cord is not fixed 
        long hours = -1;
        if (Objects.nonNull(fixedAt)) {
            hours = createdAt.until( fixedAt, ChronoUnit.HOURS );
        }
        return hours;
    }

}
