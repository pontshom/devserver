package za.co.sb.devserver.rest;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.eclipse.microprofile.metrics.MetricUnits;
import org.eclipse.microprofile.metrics.annotation.Counted;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import za.co.sb.devserver.clients.devops.DevOpsAndonCord;
import za.co.sb.devserver.clients.devops.Status;
import za.co.sb.devserver.rest.vo.StatisticsVO;
import za.co.sb.devserver.services.AndonCordService;
import za.co.sb.devserver.services.BuildService;

/**
 * This Class represents the AndonCordResource object and all operations
 * available on it
 *
 * @author steven
 */
@ApplicationScoped
@Path("andoncord")
public class AndonCordResource {

    Logger log = Logger.getLogger("AndonCordResource");

    @Inject
    @RestClient
    DevOpsAndonCord devOpsAndonCordClient;

    @Inject
    AndonCordService andonCordService;
    
    @Inject
    BuildService buildService;

    @GET
    @Path("status")
    @Produces(MediaType.APPLICATION_JSON)
    @Counted(unit = MetricUnits.NONE,
            name = "andonCordStatusCheckCount",
            absolute = true,
            monotonic = true,
            displayName = "Andon Cord Status Check Count",
            description = "Metrics to show how many times andoncord/status method was called.",
            tags = {"andoncord=status"})
    @Operation(
            summary = "This service is used to determine the current state of the Andon Cord.",
            description = "Invoke this service to retrieve the current state of the Andon Cord. The state will be pushed to registered servers, "
            + "so using this service is not mandatory, but could be useful.")
    @APIResponse(responseCode = "200",
            description = "Current status of the Andon Cord.",
            content = @Content(
                    mediaType = "application/json",
                    schema = @Schema(
                            type = SchemaType.OBJECT,
                            implementation = Status.class)))
    public Status getStatus() {
        try {            
            return devOpsAndonCordClient.getStatus();
        }
        catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
            return andonCordService.getCachedState();
        }
    }
    
    // adding the new operation to this resoure/service since i feel they do related operations 
    @GET
    @Path("stats")
    @Produces(MediaType.APPLICATION_JSON)
    @Counted(unit = MetricUnits.NONE,
            name = "getBuildStatsCount",
            absolute = true,
            monotonic = true,
            displayName = "Andon Cord Get statistics Count",
            description = "Metrics to show how many times andoncord/stats method was called.",
            tags = {"andoncord=stats"})
    @Operation(
            summary = "This service is used to get statistics of the Andon Cord.")
    @APIResponse(responseCode = "200",
            description = "statistics of the build/Andon Cord.",
            content = @Content(
                    mediaType = "application/json",
                    schema = @Schema(
                            type = SchemaType.OBJECT,
                            implementation = StatisticsVO.class)))
    public StatisticsVO getBuildStatistics(@QueryParam("hours") long hours) {
        try {
            return buildService.getStats(hours);
        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
        }

        return null;
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(
            summary = "Use this service to force a change to the AndonCord.",
            description = "This service should be used for testing or when the DevOps "
            + "AndonCord server is down. Example {\n"
            + "  \"andon_cord_status\": \"true\",\n"
            + "  \"broken_builds\": [\"payments - development\", \"liquibase - master\"]\n"
            + "}")
    @APIResponse(responseCode = "202",
            description = "Forces the state of the Andon Cord. Notification only goes out "
            + "to the registered servers is the state presented is different "
            + "to the current state")
    @RequestBody(name = "Status",
            content = @Content(
                    mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(
                            type = SchemaType.OBJECT,
                            implementation = Status.class)))
    @Counted(unit = MetricUnits.NONE,
            name = "changeAndonCordStatusCount",
            absolute = true,
            monotonic = true,
            displayName = "Andon Cord Status Forced Change Count",
            description = "Metrics to show how many times andoncord/status change method was called.")
    public Response changeStatus(Status status) {
        log.info("changeStatus called");
        andonCordService.changeCordState(status);
        return Response.accepted().build();
    }
}
