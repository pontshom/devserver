/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.sb.devserver.rest.vo;

import java.util.List;
import java.util.stream.Stream;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

/**
 *
 * @author pontsho
 */
public class StatisticsVO {
    
    @Schema(description = "Percentage of a provided period the andon cord is been pulled E.g. 50% of the last 24 hours", name = "broken_builds", example = "Use the description example here")
    private String  pulledPercentage;

    @Schema(description = "Percentage of a provided period the andon cord is been up E.g. 50% of the last 24 hours", name = "broken_builds", example = "Use the description example here")
    private String  raisedPercentage;
    
    @Schema(description = "An List of Strings, each representing builds/projects that where responsible during that period", name = "brokenBuilds", example = "Use the description example here")
    private List<String> brokenBuilds; 
    
    @Schema(description = "An ordered List of Strings, of which builds/projects where the most responsible (Number of times they pulled the cord). E.g. Payments pulled the cord twice", name = "mostResponsibleBuilds", example = "Use the description example here")
    private List<String> mostResponsibleBuilds; 
    
    @Schema(description = "List of projects and time, ordered by how long the cord was up/time to repair for each project", name = "buildsOrderedByRepairTime", example = "Use the description example here")
    private List<String> buildsOrderedByRepairTime; 

    public String getPulledPercentage() {
        return pulledPercentage;
    }

    public void setPulledPercentage(String pulledPercentage) {
        this.pulledPercentage = pulledPercentage;
    }

    public String getRaisedPercentage() {
        return raisedPercentage;
    }

    public void setRaisedPercentage(String raisedPercentage) {
        this.raisedPercentage = raisedPercentage;
    }

    public List<String> getBrokenBuilds() {
        return brokenBuilds;
    }

    public void setBrokenBuilds(List<String> brokenBuilds) {
        this.brokenBuilds = brokenBuilds;
    }

    public List<String> getMostResponsibleBuilds() {
        return mostResponsibleBuilds;
    }

    public void setMostResponsibleBuilds(List<String> mostResponsibleBuilds) {
        this.mostResponsibleBuilds = mostResponsibleBuilds;
    }

    /**
     * @return the buildsOrderedByRepairTime
     */
    public List<String> getBuildsOrderedByRepairTime() {
        return buildsOrderedByRepairTime;
    }

    /**
     * @param buildsOrderedByRepairTime the buildsOrderedByRepairTime to set
     */
    public void setBuildsOrderedByRepairTime(List<String> buildsOrderedByRepairTime) {
        this.buildsOrderedByRepairTime = buildsOrderedByRepairTime;
    }
    
    
}
