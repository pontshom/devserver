package za.co.sb.devserver.services;

import za.co.sb.devserver.rest.vo.NotificationResult;
import za.co.sb.devserver.rest.vo.DeleteResult;
import za.co.sb.devserver.clients.feature.AndonCordServerDetails;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import za.co.sb.devserver.entity.Register;
import za.co.sb.devserver.rest.vo.ServiceVO;

/**
 * The Register Service holds the business logic for working with Registered
 * Services
 *
 * @author steven
 */
@Singleton
public class RegisterService {

    private static final String FINDALL = "Register.findAll";

    Logger log = Logger.getLogger("RegisterService");

    @PersistenceContext(unitName = "DevServer_PU")
    private EntityManager em;

    @Inject
    MailService mailService;

    @Inject
    NotificationService notificationService;

    @Inject
    AndonCordService andonCordService;

    Map<String, Register> listOfServers = new LinkedHashMap<>();

    /**
     * Saves the list to the DB
     */
    public void persistList() {
        em.createNamedQuery("Register.deleteAll").executeUpdate();
        for (Register reg : listOfServers.values()) {            
            em.persist(reg);
        }
    }
    
    @PostConstruct
    public void initialise() {
        List<Register> listReg = em.createNamedQuery(FINDALL, Register.class).getResultList();
        for (Register reg: listReg) {
            listOfServers.put(reg.getServiceName(), reg);
        }        
    }

    /**
     * Add a service or update an existing service.
     * @param reg The Service to register or update
     * @throws ValidationException 
     */
    public void createOrUpdateRegister(Register reg) throws ValidationException {
        Set violations = validateRegister(reg);
        if (!violations.isEmpty()) {
            throw new ValidationException(violations);
        }
        listOfServers.put(reg.getServiceName(), reg);
        log.log(Level.INFO, "Updating Registered Service [{0}] with new values", reg.getServiceName());
        andonCordService.notifyServerOfCurrentStatus(reg);
        notifyRegisteredServerListOfAChange();
    }

    private Set validateRegister(Register reg) {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<Register>> constraintViolations = validator.validate(reg);

        if (!constraintViolations.isEmpty()) {
            Set<String> violationMessages = new HashSet<>();

            constraintViolations.forEach(constraintViolation
                    -> violationMessages.add(constraintViolation.getPropertyPath() + ": " + constraintViolation.getMessage()));

            return violationMessages;
        }
        return Collections.emptySet();
    }

    /**
     * Notify the list of servers of a change to the list
     */
    public void notifyRegisteredServerListOfAChange() {
        List<Register> registeredServers = getAllRegisteredServers();
        AndonCordServerDetails[] sequence = getServerNotificationSequence(registeredServers);
        List<String> results = notifyServers(registeredServers, sequence);
        if (!results.isEmpty()) {
            for (String server : results) {
                deregisterFailingServer(server, "Unabled to notify server [" + server + "] of change in list of registered servers.");
            }
            notifyRegisteredServerListOfAChange();
        }
    }

    /**
     * Finds and returns a list of all the registered servers
     *
     * @return
     */
    public List<Register> getAllRegisteredServers() {
        return listOfServers.values().stream().collect(Collectors.toList());        
    }

    public DeleteResult delete(String serverName, boolean force) {
        DeleteResult delRes = new DeleteResult();
        delRes.setServerName(serverName);
        if (serverName == null) {
            delRes.setErrorDesc("Server Name cannot be null");
        } else {
            String error = deleteServer(serverName, "Removal Requested by external party", force);
            delRes.setErrorDesc(error);
            if (error == null || error.isEmpty()) {
                notifyRegisteredServerListOfAChange();
            }
        }
        return delRes;
    }

    private String deleteServer(String serverName, String reason, boolean force) {
        log.log(Level.INFO, "Received request to delete server {0}", serverName);
        Register existingReg = listOfServers.get(serverName);
        if (existingReg != null) {
            if (force) {
                log.log(Level.INFO, "Forcing delete because force is set to {0}", force);
                listOfServers.remove(serverName);
                log.log(Level.INFO, "Deleting Registered Service [{0}]", existingReg.getServiceName());
                mailService.mailNoticeOfServerRemoval(existingReg, reason);
            } else {
                log.log(Level.INFO, "Not forcing delete because force is set to {0}", force);
                mailService.mailNoticeOfAttemptedServerRemoval(existingReg, getServerNotificationSequence(getAllRegisteredServers()));
            }
        } else {
            log.info("Service not found - throw error  ");
            return "Service not found to delete [" + serverName + "]";
        }
        return null;
    }

    private AndonCordServerDetails[] getServerNotificationSequence(List<Register> registeredServers) {
        if (registeredServers == null) {
            return new AndonCordServerDetails[0];
        }
        return determineOrderingOfCordNotification(registeredServers);
    }

    /**
     * From the list of registered servers, determine the notification sequence
     *
     * @param servers
     * @return
     */
    private AndonCordServerDetails[] determineOrderingOfCordNotification(List<Register> servers) {
        AndonCordServerDetails[] sequence;
        sequence = servers.stream()
                .sorted(Comparator.comparing(Register::getLocation))
                .map(x -> {
                    AndonCordServerDetails acServer = new AndonCordServerDetails();
                    acServer.setNotificationURL(x.getAndonCordStateChangeUrl().toString());
                    acServer.setServerName(x.getServiceName());
                    return acServer;
                })
                .toArray(AndonCordServerDetails[]::new);
        return sequence;
    }

    /**
     * Delete a server from the Servers store/database
     *
     * @param serverName
     */
    public void deregisterFailingServer(String serverName, String reason) {
        deleteServer(serverName, reason, true);
        log.log(Level.INFO, "Deleting [{0}]", serverName);
    }

    /**
     * Notify all registered servers of the new sequence of registered servers.
     *
     * @param sequence
     * @return
     */
    private List<String> notifyServers(List<Register> registeredServers, AndonCordServerDetails[] sequence) {
        Map<Register, NotificationResult> results = new HashMap<>();
        List<String> serversInError = new ArrayList<>();
        for (Register server : registeredServers) {
            NotificationResult notRes = notificationService.notifyServerOfSequenceChange(server, sequence);
            results.put(server, notRes);
        }
               

        for (Register service : results.keySet()) {
            NotificationResult notRes = results.get(service);
            try {                                
                if (!notRes.isSuccessful()) {
                    serversInError.add(notRes.getServerName());
                }                           
                //log.log(Level.SEVERE, "Timeout Exception waiting for server [{0}] to respond to notification of server list.", new Object[] {service.getServiceName()});                
            }
            catch (Exception ex) {
                mailService.mailNewServerListNotificationFailed(service);
                mailService.mailExceptionToOwner(ex);
                Logger.getLogger(RegisterService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return serversInError;
    }

    /**
     * Determines the first server in the list for notification of a cord change.
     * @return The first server that should be notified.
     */
    public AndonCordServerDetails getFirstServerForNotification() {        
        List<Register> registeredServers = getAllRegisteredServers();

        if (!registeredServers.isEmpty()) {
            AndonCordServerDetails[] sequence = getServerNotificationSequence(registeredServers);
            if (sequence != null) {
                log.log(Level.INFO, "The first server to notify is {0}", new Object[] {sequence[0].getServerName()});
                return sequence[0];
            }
        }
        return null;
    }

    public List<ServiceVO> getListOfServers() {        
        List<Register> registeredServers = getAllRegisteredServers();
        return registeredServers.stream()
                .map(x -> {
                    ServiceVO reg = new ServiceVO();
                    reg.setServiceName(x.getServiceName());
                    reg.setAndonCordStateChangeUrl(x.getAndonCordStateChangeUrl().toString());
                    reg.setEmail(x.getEmail());
                    reg.setLocation(x.getLocation());
                    reg.setMetricsUrl(x.getMetricsUrl().toString());
                    reg.setRegistrationUrl(x.getRegistrationUrl().toString());
                    reg.setHealthCheckUrl(x.getHealthCheckUrl().toString());
                    return reg;
                })
                .collect(Collectors.toList());
    }

}
