package za.co.sb.devserver.services;


import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import za.co.sb.devserver.clients.devops.Status;
import za.co.sb.devserver.entity.Build;
import za.co.sb.devserver.rest.vo.BuildVO;
import za.co.sb.devserver.rest.vo.StatisticsVO;

/**
 * The Build Service holds the business logic for working with projects/builds
 *
 * @author pontsho
 */
@Stateless
public class BuildService {

    Logger log = Logger.getLogger("BuildService");

    @PersistenceContext(unitName = "DevServer_PU")
    private EntityManager em;
    
    private static final String FINDALLFORPERIOD = "Build.findAllBuildsForPeriod";
    private static final String FINDBROKENBUILDS = "Build.findBrokenBuilds";
    private static final String FINDBUILDSFORSPECIFIEDPREDIOD = "Build.findAllBuildsForPeriod";

    /**
     * Saves or updates BUILD in the DB
     * 
     */
    public void save(Build build) {
        if(Objects.nonNull(build.getId())){
            em.merge(build);
        } else {
            em.persist(build);
        }
    }
    
    /**
     * Check status and create new build or fix broken builds
     * 
     */
    public void createOrUpdateBuilds(Status status) {
        
        if(status.getAndon_cord_status()){
           for (String broken_build: status.getBroken_builds()) {
               Build build = new Build();
               build.setServiceName(broken_build);
               build.setCreatedAt(Timestamp.valueOf(LocalDateTime.now()));
               build.setFixedAt(null);
               build.setBroken(status.getAndon_cord_status());
               save(build);
            }   
        } else{
            List<Build> brokenBuilds = em.createNamedQuery(FINDBROKENBUILDS, Build.class).getResultList();
            for (Build build: brokenBuilds) {
                build.setBroken(status.getAndon_cord_status());
                build.setFixedAt(Timestamp.valueOf(LocalDateTime.now()));
                save(build);
            } 
        }             
    }
    
    public StatisticsVO getStats(long hours){
        LocalDateTime now = LocalDateTime.now();
        // select all builds broken or fixed... so that we can get percentage for both raised/pulled builds
        Query query = em.createNamedQuery(FINDBUILDSFORSPECIFIEDPREDIOD);
        // using hours passed to this function to get the start date and time 
        query.setParameter("startTimestamp", Timestamp.valueOf(now.minus(Duration.ofHours(hours))));
        query.setParameter("endTimestamp", Timestamp.valueOf(now));

        List<Build> builds = query.getResultList();

        StatisticsVO statisticsVO = new StatisticsVO();
        // set Pulled %
        statisticsVO.setPulledPercentage(calculatePercentage(builds
                .stream()
                .filter(c -> c.getBroken()).count(), builds.stream().count()) + "%");
        // set raised %
        statisticsVO.setRaisedPercentage(calculatePercentage(builds
                .stream()
                .filter(c -> c.getBroken()).count(), builds.stream().count()) + "%");
        //set builds/projects where responsible during that period
        statisticsVO.setBrokenBuilds(builds.stream()
                .map(build -> build.getServiceName())
                .collect(Collectors.toList())
                .stream().distinct()
                .collect(Collectors.toList()));
        // set ordered list of which builds/projects where the most responsible (Number of times they pulled the cord). E.g. Payments pulled the cord twice        
        statisticsVO.setMostResponsibleBuilds(buildMostResponsibleBuildsList(builds.stream()
                .map(build -> build.getServiceName())
                .collect(Collectors.toList())));
        // set a list of projects and time, ordered by how long the cord was up/time to repair for each project
        statisticsVO.setBuildsOrderedByRepairTime(getListOfBuildVOsOrderedByRepairTime(builds));

        return statisticsVO;
    }
    
    private double calculatePercentage(long count, long total) {
        return (count / total) * 100;
    }
    
    private List<String> buildMostResponsibleBuildsList(List<String> items) {
        List<String> finalOrderedList = new ArrayList<>();
        Map<String, Long> result
                = items.stream()
                        .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        //now do the sorting of the map and add to finalOrderedList
        result.entrySet().stream()
                .sorted(Map.Entry.<String, Long>comparingByValue()
                        .reversed()).forEachOrdered(e -> {
                    finalOrderedList.add(e.getKey() + " pulled the cord " + e.getValue() + " times");
                });

        return finalOrderedList;
    }
    
    //add a getRepairTime on BuildVO to help us sort with hours it took to get the build fixed 
    private List<String> getListOfBuildVOsOrderedByRepairTime(List<Build> builds) {
        return builds.stream()
                .map(build -> {
                    BuildVO vo = new BuildVO();
                    vo.setServiceName(build.getServiceName());
                    vo.setBroken(build.getBroken());
                    vo.setCreatedAt(build.getCreatedAt().toLocalDateTime());
                    vo.setFixedAt(Objects.nonNull(build.getFixedAt()) ? build.getFixedAt().toLocalDateTime() : null);
                    return vo;
                }).sorted(Comparator.comparingLong(BuildVO::getRepairTime)
                .reversed())
                .map(build -> build.getServiceName()+ " repaired in " + build.getRepairTime() + "hours")
                .collect(Collectors.toList());
    }

}
