package za.co.sb.devserver.services;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Asynchronous;
import javax.ejb.Singleton;
import javax.inject.Inject;
import za.co.sb.devserver.clients.devops.Status;
import za.co.sb.devserver.clients.feature.AndonCordServerDetails;
import za.co.sb.devserver.entity.Register;

/**
 *
 * @author steven
 */
@Singleton
public class AndonCordService {

    Logger log = Logger.getLogger("AndonCordService");

    @Inject
    NotificationService notificationService;

    @Inject
    RegisterService registerService;

    @Inject
    MailService mailService;
    
    @Inject
    BuildService buildService;

    boolean lastStatus = false;
    String[] brokenBuilds;

    /**
     * Notifies the first server in the list of the state change.
     */
    @Deprecated
    private void notifyServerOfChange() {
        log.info("Notify First server of Andon Cord state change called");
        AndonCordServerDetails acServer = registerService.getFirstServerForNotification();
        if (acServer != null) {
            log.log(Level.INFO, "--> First server to notify found [{0}]", acServer.getServerName());
            log.log(Level.INFO, "---> Notifying for state change at URL {0}", acServer.getNotificationURL());
            Status statusObj = new Status();
            statusObj.setAndon_cord_status(lastStatus);
            statusObj.setBroken_builds(brokenBuilds);
            try {
                notificationService.notifyAndonCord(statusObj, new URL(acServer.getNotificationURL()));
            } catch (UnableToNotifyException | MalformedURLException ex) {
                log.log(Level.INFO, "Server {0} not responding to notification. Removing from registered list", acServer.getServerName());
                registerService.deregisterFailingServer(acServer.getServerName(), "Unable to notify server of a change in the Andon Cord state.");
                registerService.notifyRegisteredServerListOfAChange();
            }
        }
    }

    /**
     * Notifies all the registered servers of the state change of the Andon Cord.
     */
    private void notifyServersOfChange() {
        log.info("Notify All servers of Andon Cord state change called");
        List<Register> servers = registerService.getAllRegisteredServers();

        for (Register server : servers) {

            log.log(Level.INFO, "--> Notifying server [{0}]", server.getServiceName());
            log.log(Level.INFO, "---> Notifying for state change at URL {0}", server.getAndonCordStateChangeUrl());
            Status statusObj = buildStatus();
            try {
                notificationService.notifyAndonCord(statusObj, server.getAndonCordStateChangeUrl());
            } catch (UnableToNotifyException ex) {
                log.log(Level.INFO, "Server {0} not responding to notification. Removing from registered list", server.getServiceName());
                registerService.deregisterFailingServer(server.getServiceName(), "Unable to notify server of a change in the Andon Cord state.");
                registerService.notifyRegisteredServerListOfAChange();
            }

        }
    }

    /**
     * Force a change to the Andon Cord status
     *
     * @param status
     */
    public void changeCordState(Status status) {
        log.info("changeCordState called");
        //from i can the log the build with this status in the DB
        buildService.createOrUpdateBuilds(status);
       
        lastStatus = status.getAndon_cord_status();
        brokenBuilds = status.getBroken_builds();
        notifyServersOfChange();
    }

    /**
     * Force a notification of the current Andon Cord status to a server
     *
     * @param status
     */
    @Asynchronous
    public void notifyServerOfCurrentStatus(Register reg) {
        log.info("notifyServerOfCurrentStatus");
        Status status = new Status();
        status.setAndon_cord_status(lastStatus);
        status.setBroken_builds(brokenBuilds);
        try {
            notificationService.notifyAndonCord(status, reg.getAndonCordStateChangeUrl());
        } catch (UnableToNotifyException uEx) {
            mailService.mailNoticeOfInitialCordSetFailed(reg);
        }

    }
    
    /**
     * Return the last known state of the Andon Cord
     * @return 
     */
    public Status getCachedState() {        
        return buildStatus();                
    }
    
    private Status buildStatus() {
        Status status = new Status();
        status.setAndon_cord_status(lastStatus);
        status.setBroken_builds(brokenBuilds);
        return status;        
    }

}
