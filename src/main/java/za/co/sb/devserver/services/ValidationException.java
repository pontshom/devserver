package za.co.sb.devserver.services;

import java.util.Set;

/**
 *
 * @author steven
 */
public class ValidationException extends Exception {
    
    private final Set<String> violations;
    
    public ValidationException(Set violations) {
        this.violations = violations;
    }
    
    public Set<String> getViolations() {
        return violations;
    }
    
    
}
