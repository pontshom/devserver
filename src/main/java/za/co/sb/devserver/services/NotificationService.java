package za.co.sb.devserver.services;

import za.co.sb.devserver.rest.vo.NotificationResult;
import java.net.URL;
import za.co.sb.devserver.clients.feature.AndonCordServerDetails;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import org.eclipse.microprofile.rest.client.RestClientBuilder;
import za.co.sb.devserver.clients.devops.Status;
import za.co.sb.devserver.clients.feature.AndonCordServerClient;
import za.co.sb.devserver.clients.feature.AndonCordServers;
import za.co.sb.devserver.entity.Register;

/**
 *
 * @author steven
 */
@Stateless
public class NotificationService {

    Logger log = Logger.getLogger("NotificationService");

    //@Asynchronous
    public NotificationResult notifyServerOfSequenceChange(Register register, AndonCordServerDetails[] sequence) {
        NotificationResult notRes = new NotificationResult();
        notRes.setServerName(register.getServiceName());
        notRes.setSuccessful(true);
        log.log(Level.INFO, "Notifying [{0}] at {1}", new Object[]{register.getServiceName(), register.getRegistrationUrl()});
        int resultStatus = notifySequence(register.getRegistrationUrl(), sequence);
        if (resultStatus != 202) {
            notRes.setSuccessful(false);
        }
        return notRes;
    }

    private int notifySequence(URL baseUrl, AndonCordServerDetails[] sequence) {
        log.info("--> Notifying of changes to sequence. Sequence is :");
        StringBuilder sb = new StringBuilder();
        sb.append("---->");
        for (AndonCordServerDetails det : sequence) {
            sb.append(det.getServerName());
            sb.append(", ");
        }
        log.log(Level.INFO, sb.toString());
        AndonCordServerClient client
                = RestClientBuilder.newBuilder()
                        .baseUrl(baseUrl)
                        .build(AndonCordServerClient.class);
        AndonCordServers acServers = new AndonCordServers();
        acServers.setAndonCordServerDetails(sequence);
        try {
            Response resp = client.register(acServers);
            return resp.getStatus();
        } catch (WebApplicationException webAppEx) {
            return webAppEx.getResponse().getStatus();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_FOUND).build().getStatus();
        }

    }

    public void notifyAndonCord(Status status, URL notficationURL) throws UnableToNotifyException {
        log.log(Level.INFO, "Notify Andon Cord called in Notification Service. Status {0} : URL {1}", new Object[]{status.getAndon_cord_status(), notficationURL});
        AndonCordServerClient client
                = RestClientBuilder.newBuilder()
                        .baseUrl(notficationURL)
                        .build(AndonCordServerClient.class);
        try {
            Response resp = client.status(status);
            if (resp.getStatus() != 200 && resp.getStatus() != 202) {
                log.log(Level.WARNING, "Notification to {0} resulted in status code {1}", new Object[]{notficationURL, resp.getStatus()});
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Unable to notify Client Service of Andon Cord change. Error is {0}", e.getMessage());
            log.log(Level.WARNING, e.getMessage(), e);
            throw new UnableToNotifyException();
        }
    }
    
}
