package za.co.sb.devserver.services.schedules;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import za.co.sb.devserver.clients.devops.DevOpsAndonCord;
import za.co.sb.devserver.clients.devops.Status;
import za.co.sb.devserver.services.AndonCordService;
import za.co.sb.devserver.services.HealthService;
import za.co.sb.devserver.services.RegisterService;

/**
 * This service runs a scheduler that checks for the state of the Andon Cord
 * via the DevOps service. If the state changes, it triggers a changeCordState on 
 * the AndonCord Service
 * @author steven
 */
@Stateless
public class ScheduleService {
    
    Logger log = Logger.getLogger("AndonCordScheduleService");

    @Inject
    @RestClient
    DevOpsAndonCord devOpsAndonCordClient;

    @Inject
    AndonCordService andonCordService;
    
    @Inject
    HealthService healthService;
    
    @Inject
    RegisterService registerService;

    boolean lastStatus = false;

    @Schedule(hour = "*", minute = "*", second = "*/5", persistent = false)
    public void checkCord() {
        try {
            Status status = devOpsAndonCordClient.getStatus();
            if (lastStatus != status.getAndon_cord_status()) {
                lastStatus = status.getAndon_cord_status();
                andonCordService.changeCordState(status);
            }
        } catch (Exception e) {
            log.log(Level.WARNING,"DevOps Andon Cord service not responding");
        }
    }
    
    @Schedule(hour = "*", minute = "30", persistent = false)
    public void checkHealth() {
        healthService.doHealthChecks();
    }
    
    @Schedule(hour = "*", minute = "10", persistent = false)
    public void persistServerList() {
        registerService.persistList();
    }
}
