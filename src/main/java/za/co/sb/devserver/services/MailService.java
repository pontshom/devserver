package za.co.sb.devserver.services;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import za.co.sb.devserver.clients.feature.AndonCordServerDetails;
import za.co.sb.devserver.clients.feature.Health;
import za.co.sb.devserver.clients.feature.HealthCheck;
import za.co.sb.devserver.entity.Register;

/**
 *
 * @author steven
 */
@Stateless
public class MailService {

    @Inject
    @ConfigProperty(name = "smtp.server.host")
    private String smtpServerHost;

    @Inject
    @ConfigProperty(name = "smtp.server.port")
    private String smtpServerPort;

    Logger log = Logger.getLogger("MailService");

    Session session;
    
    static final String SERVICE_NAME = "DevService";
    static final String OH_DEAR = "Oh Dear";

    @PostConstruct
    private void init() {
        session = createSession();
    }

    public void mailNoticeOfServerRemoval(Register reg, String reason) {
        log.log(Level.INFO, "Sending notice of removal email.");

        // Now set the actual message
        String messageText = "Hello " + reg.getServiceName() + "\n"
                + "\n"
                + "Your service has been removed from the list of registered servers. \n"
                + "The reason for this is: " + reason
                + "\n\n"
                + "Regards, "
                + "\n"
                + SERVICE_NAME;

        sendMail(reg.getEmail(), "DevService Notification - Server DelServer Removed", messageText);

    }

    private void sendMail(String toEmail, String subject, String messageText) {
        log.log(Level.INFO, "Sending mail to {0}, using smtp server + {1}", new Object[]{toEmail, smtpServerHost});

        // Sender's email ID needs to be mentioned
        String from = "noreply-DevService@standardbank.co.za";

        try {
            // Create a default MimeMessage object.
            Message message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(toEmail));

            // Set Subject: header field
            message.setSubject(subject);

            // Now set the actual message
            message.setText(messageText);

            // Send message
            Transport.send(message);

            log.log(Level.INFO, "Sent message successfully....");
        } catch (MessagingException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    private Session createSession() {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "false");
        props.put("mail.smtp.starttls.enable", "false");
        props.put("mail.smtp.host", smtpServerHost);
        props.put("mail.smtp.port", smtpServerPort);

        // Get the Session object.
        return Session.getInstance(props);
    }

    public void notifyOfHealthIssues(Register reg, Health health) {
        log.log(Level.INFO, "Sending health check down email.");

        StringBuilder sb = new StringBuilder();
        for (HealthCheck healthCheck : health.getChecks()) {
            sb.append(healthCheck.getName()).append(" : ").append(healthCheck.getState()).append("\n");
            StringBuilder dataBuilder = new StringBuilder();
            for (String value : healthCheck.getData().keySet()) {
                dataBuilder.append("   ").append(value).append(" ").append(healthCheck.getData().get(value)).append("\n");
            }
            sb.append(dataBuilder.toString());
        }
        // Now set the actual message
        String messageText = OH_DEAR + " " + reg.getServiceName() + "\n"
                + "\n"
                + "Your service isn't doing all that well.\n"
                + "When I asked, your health check returned a " + health.getOutcome() + " result.\n"
                + "The details that you supplied to me are below.\n\n"
                + sb.toString()
                + "\n\n"
                + "Regards,\nDevService";

        sendMail(reg.getEmail(), "DevService Notification - Health check - down", messageText);

    }

    public void notifyOfHealthIssues(Register reg, int status) {
        log.log(Level.INFO, "Sending health check status code error email.");

        // Now set the actual message
        String messageText = "Oh dear " + reg.getServiceName() + "\n"
                + "\n"
                + "Your service isn't responding to my health checks.\n"
                + "When I asked, your service responded with a status code of " + status + "\n"
                + "\n"
                + "\n"
                + "Regards,\n"
                + SERVICE_NAME;

        sendMail(reg.getEmail(), "DevService Notification - Health check - status", messageText);

    }

    public void notifyOfHealthIssues(Register reg, Exception e) {
        log.log(Level.INFO, "Sending health check exception email.");

        OutputStream out = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(out);
        e.printStackTrace(ps);
        
        
        // Now set the actual message
        String messageText = "Oh dear " + reg.getServiceName() + "\n"
                + "\n"
                + "Your service isn't responding to my health checks.\n"
                + "When I asked, your service caused an exception with message " + e.getMessage() + "\n"
                +  out.toString() + "/n"
                + "\n"
                + "\n"
                + "Regards, \n"
                + SERVICE_NAME;

        sendMail(reg.getEmail(), "DevService Notification - Health check - exception", messageText);

    }
    
    public void notifyOfHealthIssues(Register reg, String message) {
        log.log(Level.INFO, "Sending health check exception email.");
                
        
        // Now set the actual message
        String messageText = "Oh dear " + reg.getServiceName() + "\n"
                + "\n"
                + "Your service isn't responding to my health checks.\n"
                + "When I asked, your service caused an exception/problem with message [" + message + "]\n"                
                + "\n"
                + "\n"
                + "Regards, \n"
                + SERVICE_NAME;

        sendMail(reg.getEmail(), "DevService Notification - Health check - exception", messageText);

    }
    
    public void mailNoticeOfInitialCordSetFailed(Register reg) {
        String messageText = "Oh boy " + reg.getServiceName() + "!\n"
                + "\n"
                + "Your service isn't responding to my Andon Cord state call.\n"
                + "When I tried to send you the current state of the Andon Cord, your service didn't respond appropriately\n"
                + "\n"
                + "\n"
                + "Yours, \n"
                + "DevService ";
        sendMail(reg.getEmail(), "DevService Notification - Setting the Initial State", messageText);
    }
    
    public void mailNoticeOfAttemptedServerRemoval(Register reg, AndonCordServerDetails[] sequence) {        
        StringBuilder sb = new StringBuilder();
        for (AndonCordServerDetails server : sequence) {
            sb.append("  ").append(server.getServerName()).append(" ").append(server.getNotificationURL()).append("\n");
        }
        
        String messageText = "Oh uh " + reg.getServiceName() + "\n"
                + "\n"
                + "Another service notified me of a problem trying to set the Andon Cord state on your service and attempted to remove you from the list.\n"
                + "They didn't force the delete, so I'm not going to remove you, but you should look into why this is happening.\n"
                + "\n"
                + "The sequenced notification list is below (You can try and work out who was attempting to call you from it.\n"
                + "\n"
                + sb.toString()
                + "\n"
                + "\n"                
                + "Regards, \n"
                + "DevService";
        sendMail(reg.getEmail(), "DevService Notification - Attempted removal", messageText);
    };

    public void mailNewServerListNotificationFailed(Register service) {
        String messageText = "Mmmm " + service.getServiceName() + "\n"
                + "\n"
                + "I'm having trouble letting you know about the new list of servers.\n"
                + "Please have a look.\n"
                + "\n"                
                + "\n"
                + "\n"                
                + "Regards, \n"
                + "DevService";
        sendMail(service.getEmail(), "DevService Notification - Unable to send new list of servers", messageText);
    }
    
    public void mailExceptionToOwner(Exception ex) {
        log.log(Level.INFO, "Sending DevServer exception to owner");

        OutputStream out = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(out);
        ex.printStackTrace(ps);
        
        // Now set the actual message
        String messageText = "Oh dear DevServer\n"
                + "\n"
                + "The following exception occured on the server.\n"
                +  out.toString() + "/n"
                + "\n"
                + "\n"
                + "Regards, \n"
                + SERVICE_NAME;

        sendMail("steven.webber@standardbank.co.za", "DevService Notification - exception", messageText);

    }
}
