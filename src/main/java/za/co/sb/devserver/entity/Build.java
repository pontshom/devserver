package za.co.sb.devserver.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author pontsho
 */
@Entity
@NamedQuery(name="Build.findByServiceName", query="SELECT b from Build b where b.serviceName = :serviceName")
@NamedQuery(name="Build.findByServiceNameAndStatus", query="SELECT b from Build b where b.serviceName = :serviceName")
@NamedQuery(name="Build.findBrokenBuilds", query="SELECT b from Build b where b.broken = true")
@NamedQuery(name="Build.findAllBuildsForPeriod", query="SELECT b from Build b where b.createdAt BETWEEN :startTimestamp AND :endTimestamp")
public class Build implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @NotNull
    private String serviceName;
    
    @NotNull
    private Boolean broken;
    
    @Column(name = "createdAt")
    //@Temporal(TemporalType.TIMESTAMP)
    private Timestamp createdAt;
    
    @Column(name = "fixedAt")
    //@Temporal(TemporalType.TIMESTAMP)
    private Timestamp fixedAt;
        
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    } 
   
    public Boolean getBroken() {
        return broken;
    }

    public void setBroken(Boolean broken) {
        this.broken = broken;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getFixedAt() {
        return fixedAt;
    }

    public void setFixedAt(Timestamp fixedAt) {
        this.fixedAt = fixedAt;
    }

     @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {        
        if (!(object instanceof Build)) {
            return false;
        }
        Build other = (Build) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "za.co.sb.devserver.entity.Build [ id=" + id + " ]";
    }

}
