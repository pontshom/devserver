/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.sb.devserver.services.it;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.extension.rest.client.ArquillianResteasyResource;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.FileAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.*;
import org.junit.runner.RunWith;
import za.co.sb.devserver.clients.devops.Status;
import za.co.sb.devserver.entity.Register;
import za.co.sb.devserver.rest.vo.RegisterResultVO;
import za.co.sb.devserver.rest.vo.ServiceVO;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

/**
 *
 * @author stevenwebber
 */
@RunWith(Arquillian.class)
public class BuildStatsServiceTest {

    @ArquillianResource
    private URL deploymentURL;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options().port(8104));

    @Deployment
    public static Archive<?> deployment() {
        WebArchive jar = ShrinkWrap.create(WebArchive.class, "test.war")
                .addClass(Register.class)
                .addPackage("za.co.sb.devserver.clients.devops")                
                .addPackage("za.co.sb.devserver.rest")
                .addPackage("za.co.sb.devserver.rest.vo")                
                .addClass("za.co.sb.devserver.services.AndonCordService")
                .addClass("za.co.sb.devserver.services.RegisterService")
                .addClass("za.co.sb.devserver.services.MailService")
                .addClass("za.co.sb.devserver.services.NotificationService")
                .addClass("za.co.sb.devserver.services.ValidationException")                
                .addClass("za.co.sb.devserver.services.UnableToNotifyException")                
                .addClass("za.co.sb.devserver.services.HealthService")
                .addClass("za.co.sb.devserver.services.BuildService")
                .addPackage("za.co.sb.devserver.entity")
                .addPackage("za.co.sb.devserver.clients.feature")
                .addAsResource(new FileAsset(new File("src/test/resources/META-INF/persistence.xml")), "META-INF/persistence.xml")
                .addAsResource(new FileAsset(new File("src/test/resources/META-INF/microprofile-config.properties")), "META-INF/microprofile-config.properties")
                .addAsWebInfResource(new FileAsset(new File("src/main/webapp/WEB-INF/web.xml")), "web.xml");        
        return jar;

    }

    public BuildStatsServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {
    }

    @Test
    //@Ignore
    @RunAsClient
    public void checkCord(@ArquillianResteasyResource("") final WebTarget webTarget) {

        WireMock.stubFor(WireMock.get(WireMock.urlEqualTo("/api/andoncord/status"))
                .willReturn(WireMock.aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"andon_cord_status\":false}")));

            Response resp = webTarget.path("devservice/andoncord/status")
                    .request()
                    .accept(MediaType.APPLICATION_JSON)
                    .buildGet().invoke();
            Assert.assertEquals(200, resp.getStatus());
            Status status = resp.readEntity(Status.class);
            Assert.assertEquals(Boolean.FALSE, status.getAndon_cord_status());
    }

    @Test
    //@Ignore
    @RunAsClient
    public void checkCordInError(@ArquillianResteasyResource("") final WebTarget webTarget) {
        WireMock.stubFor(WireMock.get(WireMock.urlEqualTo("/api/andoncord/status"))
                .willReturn(WireMock.aResponse().withStatus(404)
                        .withHeader("Content-Type", "application/json")));
        //.withBody("{\"andon_cord_status\":false}")));


        Response resp = webTarget.path("devservice/andoncord/status")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildGet().invoke();
        // Should return the catched status of the cord
        Assert.assertEquals(200, resp.getStatus());
    }

    @Test
    @RunAsClient
    public void checkCordStatusChange(@ArquillianResteasyResource("") final WebTarget webTarget) {
        
        WireMock.stubFor(WireMock.get(WireMock.urlEqualTo("/api/andoncord/status"))
                .willReturn(WireMock.aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"andon_cord_status\":false}")));
        
        WireMock.stubFor(WireMock.put(WireMock.urlEqualTo("/services"))
                .willReturn(WireMock.aResponse().withStatus(202)
                        .withHeader("Content-Type", "application/json")));

        WireMock.stubFor(WireMock.put(WireMock.urlEqualTo("/service1/andoncord"))                
                .willReturn(WireMock.aResponse().withStatus(200)
                        .withHeader("Content-Type", "application/json")));
        
        WireMock.stubFor(WireMock.put(WireMock.urlEqualTo("/service2/andoncord"))                
                .willReturn(WireMock.aResponse().withStatus(200)                       
                        .withHeader("Content-Type", "application/json")));

        ServiceVO reg = buildValidServiceRegisterVO("Service1", "B", "http://localhost:8104/service1");

        Entity regEntity = Entity.entity(reg, MediaType.APPLICATION_JSON);
        //System.out.println("########################################");
        //System.out.println(webTarget.getUri().getPath()); //test/rest why?
        //System.out.println("########################################");
        Response resp = webTarget.path("devservice/services")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildPut(regEntity).invoke();        
        Assert.assertEquals(202, resp.getStatus());

        ServiceVO reg2 = buildValidServiceRegisterVO("Service2", "A", "http://localhost:8104/service2");
        Entity regEntity2 = Entity.entity(reg2, MediaType.APPLICATION_JSON);
        Response resp2 = webTarget.path("devservice/services")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildPut(regEntity2).invoke();
        
        RegisterResultVO regResult = resp.readEntity(RegisterResultVO.class);
        Assert.assertEquals(true, regResult.isRegistered());
        
        // The real test

        Status status = new Status();
        status.setAndon_cord_status(Boolean.TRUE);
        status.setBroken_builds(new String[] {"liquibase - development", "payments - master"});

        Entity statusEntity = Entity.entity(status, MediaType.APPLICATION_JSON);

        Response actResp = webTarget.path("devservice/andoncord")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildPut(statusEntity)
                .invoke();
        
        WireMock.verify(WireMock.putRequestedFor(WireMock.urlEqualTo("/service2/andoncord"))
                .withRequestBody(WireMock.equalToJson("{\"andon_cord_status\":true,\"broken_builds\":[\"liquibase - development\", \"payments - master\"]}")));
    }
    
    private ServiceVO buildValidServiceRegisterVO(String serviceName, String location, String andonCordStatusChangeURL) {
        ServiceVO reg = new ServiceVO();
        reg.setAndonCordStateChangeUrl(andonCordStatusChangeURL);
        reg.setRegistrationUrl("http://localhost:8104");
        reg.setServiceName(serviceName);
        reg.setEmail("steven.webber@standardbank.co.za");
        reg.setMetricsUrl("http://localhost:8104/metrics");
        reg.setLocation(location);
        reg.setHealthCheckUrl("http://localhost:8104/");
        return reg;
    }
        
}
