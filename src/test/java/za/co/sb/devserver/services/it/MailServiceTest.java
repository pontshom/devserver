/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.sb.devserver.services.it;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetupTest;
import fish.payara.arquillian.inject.Inject;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.extension.rest.client.ArquillianResteasyResource;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.FileAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.*;
import org.junit.runner.RunWith;
import za.co.sb.devserver.entity.Register;
import za.co.sb.devserver.rest.vo.DeleteResult;
import za.co.sb.devserver.rest.vo.ServiceVO;
import za.co.sb.devserver.services.MailService;

import javax.mail.MessagingException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.net.URL;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.junit.Assert.*;

/**
 * Tests the email capabilities of the system and that notifications do go out.
 *
 * @author steven
 */
@RunWith(Arquillian.class)
public class MailServiceTest {

    @Inject
    MailService mailService;

    @ArquillianResource
    private URL deploymentURL;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options().port(8104));

    GreenMail greenMail;

    @Deployment
    public static Archive<?> deployment() {
        WebArchive jar = ShrinkWrap.create(WebArchive.class, "test.war")
                .addClass(Register.class)
                .addPackage("za.co.sb.devserver.clients.devops")
                .addPackage("za.co.sb.devserver.entity")
                .addPackage("za.co.sb.devserver.clients.feature")
                .addPackage("za.co.sb.devserver.services")
                .addClass("za.co.sb.devserver.rest.DevServiceApplication")
                .addClass("za.co.sb.devserver.rest.ServiceResource")
                .addClass("za.co.sb.devserver.services.BuildService")
                .addClass("za.co.sb.devserver.rest.BeanViolationExceptionMapper")
                .addPackage("za.co.sb.devserver.rest.vo")
                .addAsResource(new FileAsset(new File("src/test/resources/META-INF/persistence.xml")), "META-INF/persistence.xml")
                .addAsResource(new FileAsset(new File("src/test/resources/META-INF/microprofile-config.properties")), "META-INF/microprofile-config.properties")
                .addAsWebInfResource(new FileAsset(new File("src/main/webapp/WEB-INF/web.xml")), "web.xml");
        return jar;
    }

    public MailServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        greenMail = new GreenMail(ServerSetupTest.SMTP);
        greenMail.start();
    }

    @After
    public void tearDown() {
        greenMail.stop();
    }

    @Test
    //@Ignore
    @RunAsClient
    public void emailForRemoveServerForced(@ArquillianResteasyResource("") final WebTarget webTarget) {

        // Register the DelService
        WireMock.stubFor(WireMock.put(WireMock.urlEqualTo("/services"))
                .willReturn(WireMock.aResponse().withStatus(202)
                        .withHeader("Content-Type", "application/json")));

        WireMock.stubFor(WireMock.put(WireMock.urlEqualTo("/andoncord"))
                .willReturn(WireMock.aResponse().withStatus(202)));

        ServiceVO reg = buildServiceRegisterVO("DelServer", "steven.webber@standardbank.co.za");
        Entity regEntity = Entity.entity(reg, MediaType.APPLICATION_JSON);
        Response resp = webTarget.path("devservice/services")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildPut(regEntity).invoke();

        Assert.assertEquals(202, resp.getStatus());

        // Send the current state to the newly registered service
        WireMock.verify(WireMock.putRequestedFor(WireMock.urlEqualTo("/andoncord")));

        // First register notification - when DelServer is added - should fire twice
        WireMock.verify(WireMock.putRequestedFor(WireMock.urlEqualTo("/services"))
                .withRequestBody(WireMock.equalToJson("{\n\"andonCordServerDetails\" : [ {\n"
                        + "    \"notificationURL\" : \"http://localhost:8104/\",\n"
                        + "    \"serverName\" : \"DelServer\"\n"
                        + "  } ]\n"
                        + "}")));

        // Invoke the Delete of the DelService
        resp = webTarget.path("devservice/services").queryParam("serviceName", "DelServer").queryParam("force", true)
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildDelete().invoke();

        Assert.assertEquals(200, resp.getStatus());
        DeleteResult delResult = resp.readEntity(DeleteResult.class);
        Assert.assertEquals("DelServer", delResult.getServerName());
        Assert.assertNull(delResult.getErrorDesc());

        // Second register notification (after delete)
        WireMock.verify(WireMock.putRequestedFor(WireMock.urlEqualTo("/services"))
                .withRequestBody(WireMock.equalToJson("{\"andonCordServerDetails\":[{\"notificationURL\":\"http://localhost:8104/\",\"serverName\":\"DelServer\"}]}")));

        // Confirm email was sent
        try {
            String subject = greenMail.getReceivedMessages()[0].getSubject();
            assertEquals("DevService Notification - Server DelServer Removed", subject);
            //subject = greenMail.getReceivedMessages()[0].getSubject();
            //assertEquals("DevService Notification - Setting the Initial State", subject);
        } catch (MessagingException mEx) {
            fail(mEx.getMessage());
        }
        greenMail.reset();
    }

    @Test
    //@Ignore
    @RunAsClient
    public void emailForRemoveServerAttempted(@ArquillianResteasyResource("") final WebTarget webTarget) {
        // Register the DelService
        WireMock.stubFor(WireMock.put(WireMock.urlEqualTo("/services"))
                .willReturn(WireMock.aResponse().withStatus(202)
                        .withHeader("Content-Type", "application/json")));

        WireMock.stubFor(WireMock.put(WireMock.urlEqualTo("/andoncord"))
                .willReturn(WireMock.aResponse().withStatus(202)));

        ServiceVO reg = buildServiceRegisterVO("DelServer", "steven.webber@standardbank.co.za");
        Entity regEntity = Entity.entity(reg, MediaType.APPLICATION_JSON);
        Response resp = webTarget.path("devservice/services")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildPut(regEntity).invoke();

        Assert.assertEquals(202, resp.getStatus());

        // Send the current state to the newly registered service
        WireMock.verify(WireMock.putRequestedFor(WireMock.urlEqualTo("/andoncord")));

        // First register notification - when DelServer is added - should fire twice
        WireMock.verify(WireMock.putRequestedFor(WireMock.urlEqualTo("/services"))
                .withRequestBody(WireMock.equalToJson("{\n\"andonCordServerDetails\" : [ {\n"
                        + "    \"notificationURL\" : \"http://localhost:8104/\",\n"
                        + "    \"serverName\" : \"DelServer\"\n"
                        + "  } ]\n"
                        + "}")));

        // Invoke the Delete of the DelService
       // webTarget.path("devservice/services").queryParam("serviceName", "DelServer");

        resp = webTarget.path("devservice/services").queryParam("serviceName", "DelServer")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildDelete().invoke();

        Assert.assertEquals(200, resp.getStatus());
        DeleteResult delResult = resp.readEntity(DeleteResult.class);
        Assert.assertEquals("DelServer", delResult.getServerName());
        Assert.assertNull(delResult.getErrorDesc());

        // Second register notification (after delete)
        WireMock.verify(WireMock.putRequestedFor(WireMock.urlEqualTo("/services"))
                .withRequestBody(WireMock.equalToJson("{\"andonCordServerDetails\":[{\"notificationURL\":\"http://localhost:8104/\",\"serverName\":\"DelServer\"}]}")));

        // Confirm email was sent
        try {
            assertNotNull(greenMail.getReceivedMessages());
            String subject = greenMail.getReceivedMessages()[0].getSubject();
            assertEquals("DevService Notification - Attempted removal", subject);

        } catch (MessagingException mEx) {
            fail(mEx.getMessage());
        }
        greenMail.reset();
    }

    private ServiceVO buildServiceRegisterVO(String name, String email) {
        return buildServiceRegisterVO(name, email, "http://localhost:8104/", "E", "http://locahost:8104/metrics", "http://localhost:8104/", "http://locahost:8104/");
    }

    private ServiceVO buildServiceRegisterVO(String name, String email, String registrationURL) {
        return buildServiceRegisterVO(name, email, "http://localhost:8104/", "E", "http://locahost:8104/metrics", registrationURL, "http://locahost:8104/");
    }

    private ServiceVO buildServiceRegisterVO(String name, String email, String andonCord, String location, String metrics, String registration, String health) {
        ServiceVO regVO = new ServiceVO();
        regVO.setAndonCordStateChangeUrl(andonCord);
        regVO.setEmail(email);
        regVO.setLocation(location);
        regVO.setMetricsUrl(metrics);
        regVO.setRegistrationUrl(registration);
        regVO.setServiceName(name);
        regVO.setHealthCheckUrl(health);
        return regVO;
    }

}
