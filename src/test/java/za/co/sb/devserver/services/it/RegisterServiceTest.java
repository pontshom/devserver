package za.co.sb.devserver.services.it;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetupTest;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.extension.rest.client.ArquillianResteasyResource;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.FileAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.*;
import org.junit.runner.RunWith;
import za.co.sb.devserver.entity.Register;
import za.co.sb.devserver.rest.vo.DeleteResult;
import za.co.sb.devserver.rest.vo.RegisterListVO;
import za.co.sb.devserver.rest.vo.RegisterResultVO;
import za.co.sb.devserver.rest.vo.ServiceVO;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import org.jboss.arquillian.junit.InSequence;
import org.junit.runners.MethodSorters;

/**
 *
 * @author stevenwebber
 */
@RunWith(Arquillian.class)
//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegisterServiceTest {

    @ArquillianResource
    private URL deploymentURL;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options().port(8104));
    
    GreenMail greenMail;

    @Deployment
    public static Archive<?> deployment() {
        WebArchive jar = ShrinkWrap.create(WebArchive.class, "test.war")
                .addClass(Register.class)
                .addPackage("za.co.sb.devserver.clients.devops")
                .addPackage("za.co.sb.devserver.entity")
                .addPackage("za.co.sb.devserver.clients.feature")
                .addPackage("za.co.sb.devserver.services")                
                .addClass("za.co.sb.devserver.rest.DevServiceApplication")
                .addClass("za.co.sb.devserver.rest.ServiceResource")
                .addClass("za.co.sb.devserver.services.BuildService")
                .addClass("za.co.sb.devserver.rest.BeanViolationExceptionMapper")
                .addPackage("za.co.sb.devserver.rest.vo")
                .addAsResource(new FileAsset(new File("src/test/resources/META-INF/persistence.xml")), "META-INF/persistence.xml")
                .addAsResource(new FileAsset(new File("src/test/resources/META-INF/microprofile-config.properties")), "META-INF/microprofile-config.properties")
                .addAsWebInfResource(new FileAsset(new File("src/main/webapp/WEB-INF/web.xml")), "web.xml");
        return jar;

    }

    public RegisterServiceTest() {

    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
     greenMail = new GreenMail ( ServerSetupTest.SMTP );
     greenMail.start ();
    }
    
    @After
    public void tearDown() {
        greenMail.stop();
    }

    @Test
    //@Ignore
    @RunAsClient
    public void register(@ArquillianResteasyResource("") final WebTarget webTarget) {
        WireMock.stubFor(WireMock.put(WireMock.urlEqualTo("/services"))
                .willReturn(WireMock.aResponse().withStatus(202)
                        .withHeader("Content-Type", "application/json")));        

        // Build the Mock for the andoncord notification
        WireMock.stubFor(WireMock.put(WireMock.urlEqualTo("/andoncord"))
                .willReturn(WireMock.aResponse().withStatus(200)
                .withHeader("Content-Type", "application/json")));

        ServiceVO reg = buildValidServiceRegisterVO("Valid Service");
        Entity regEntity = Entity.entity(reg, MediaType.APPLICATION_JSON);
        Response resp = webTarget.path("devservice/services")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildPut(regEntity).invoke();

        Assert.assertEquals(202, resp.getStatus());
        RegisterResultVO regResult = resp.readEntity(RegisterResultVO.class);
        Assert.assertEquals(true, regResult.isRegistered());
        Assert.assertNull(regResult.getErrors());

        WireMock.verify(WireMock.putRequestedFor(WireMock.urlEqualTo("/services"))
                .withRequestBody(WireMock.matchingJsonPath("$.andonCordServerDetails[?(@.notificationURL == 'http://localhost:8104/')]")));
    }

    @Test
    //@Ignore
    @RunAsClient
    public void registerServerThatDoesntRespond(@ArquillianResteasyResource("") final WebTarget webTarget) {
        WireMock.stubFor(WireMock.put(WireMock.urlEqualTo("/noResponse/services"))
                .willReturn(WireMock.aResponse().withStatus(404)
                        .withHeader("Content-Type", "application/json")));

        // for the renotification to the "Valid Service" of the new sequence
        // Called twice, the second time because the noResponse service was removed.
        WireMock.stubFor(WireMock.put(WireMock.urlEqualTo("/services"))
                .willReturn(WireMock.aResponse().withStatus(202)
                        .withHeader("Content-Type", "application/json")));
        
        // Build the Mock for the andoncord notification
        WireMock.stubFor(WireMock.put(WireMock.urlEqualTo("/andoncord"))
                .willReturn(WireMock.aResponse().withStatus(200)));

        ServiceVO reg = buildServiceRegisterVO("Code 404 Service", "steven.webber@standardbank.co.za", "http://localhost:8104/noResponse");
        Entity regEntity = Entity.entity(reg, MediaType.APPLICATION_JSON);
        Response resp = webTarget.path("devservice/services")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildPut(regEntity).invoke();

        Assert.assertEquals(202, resp.getStatus());
        RegisterResultVO regResult = resp.readEntity(RegisterResultVO.class);
        Assert.assertEquals(true, regResult.isRegistered());
        Assert.assertNull(regResult.getErrors());
    }

    @Test
    //@Ignore
    @RunAsClient
    public void registerServerWithEmailErrorRespond(@ArquillianResteasyResource("") final WebTarget webTarget) {

        ServiceVO reg = buildServiceRegisterVO("Email Error Service", "NotAValidEmail", "http://localhost:8104/withErrors");
        Entity regEntity = Entity.entity(reg, MediaType.APPLICATION_JSON);
        Response resp = webTarget.path("devservice/services")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildPut(regEntity).invoke();

        Assert.assertEquals(400, resp.getStatus());
        RegisterResultVO regResult = resp.readEntity(RegisterResultVO.class);
        Assert.assertEquals(false, regResult.isRegistered());
        Assert.assertEquals(1, regResult.getErrors().length);
        Assert.assertArrayEquals(new String[]{"Email address is invalid in property email"}, regResult.getErrors());
    }

    @Test
    //@Ignore
    @RunAsClient
    public void registerServerWithErrorsRespond(@ArquillianResteasyResource("") final WebTarget webTarget) {

        ServiceVO reg = buildServiceRegisterVO("Email Error Service", "NotAValidEmail", null);
        Entity regEntity = Entity.entity(reg, MediaType.APPLICATION_JSON);
        Response resp = webTarget.path("devservice/services")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildPut(regEntity).invoke();

        Assert.assertEquals(400, resp.getStatus());
        RegisterResultVO regResult = resp.readEntity(RegisterResultVO.class);
        Assert.assertEquals(false, regResult.isRegistered());
        Assert.assertEquals(2, regResult.getErrors().length);
        Assert.assertArrayEquals(new String[]{"Email address is invalid in property email", "must not be null in property registrationUrl"}, regResult.getErrors());
    }

    @Test
    //@Ignore
    @RunAsClient
    public void registerServerWithURLErrorRespond(@ArquillianResteasyResource("") final WebTarget webTarget) {
         // Build the Mock for the andoncord notification
        WireMock.stubFor(WireMock.put(WireMock.urlEqualTo("/andoncord"))
                .willReturn(WireMock.aResponse().withStatus(200)
                .withHeader("Content-Type", "application/json")));

        ServiceVO reg = buildServiceRegisterVO("URL Error Service", "steven.webber@standardbank.co.za", "notAValidURL");
        Entity regEntity = Entity.entity(reg, MediaType.APPLICATION_JSON);
        Response resp = webTarget.path("devservice/services")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildPut(regEntity).invoke();

        Assert.assertEquals(400, resp.getStatus());
        RegisterResultVO regResult = resp.readEntity(RegisterResultVO.class);
        Assert.assertEquals(false, regResult.isRegistered());
        Assert.assertEquals(1, regResult.getErrors().length);
        Assert.assertArrayEquals(new String[]{"no protocol: notAValidURL"}, regResult.getErrors());
    }

    @Test
    //@Ignore
    @RunAsClient
    public void deleteRegisteredServer(@ArquillianResteasyResource("") final WebTarget webTarget) {
        // Register the DelService
        WireMock.stubFor(WireMock.put(WireMock.urlEqualTo("/services"))
                .willReturn(WireMock.aResponse().withStatus(202)
                        .withHeader("Content-Type", "application/json")));
        
         // Build the Mock for the andoncord notification
        WireMock.stubFor(WireMock.put(WireMock.urlEqualTo("/andoncord"))
                .willReturn(WireMock.aResponse().withStatus(200)
                .withHeader("Content-Type", "application/json")));

        ServiceVO reg = buildValidServiceRegisterVO("DelServerR");
        Entity regEntity = Entity.entity(reg, MediaType.APPLICATION_JSON);
        Response resp = webTarget.path("devservice/services")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildPut(regEntity).invoke();

        Assert.assertEquals(202, resp.getStatus());
        
        // First register notification - when DelServer is added - should fire twice
        WireMock.verify(WireMock.putRequestedFor(WireMock.urlEqualTo("/services"))
                .withRequestBody(WireMock.equalToJson("{\n"
                        + "  \"andonCordServerDetails\" : [ {\n"
                        + "    \"notificationURL\" : \"http://localhost:8104/\",\n"
                        + "    \"serverName\" : \"Valid Service\"\n"
                        + "  }, {\n"
                        + "    \"notificationURL\" : \"http://localhost:8104/\",\n"
                        + "    \"serverName\" : \"DelServerR\"\n"
                        + "  } ]\n"
                        + "}")));

        // Invoke the Delete of the DelService       
        resp = webTarget.path("devservice/services").queryParam("serviceName", "DelServerR").queryParam("force", true)
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildDelete().invoke();

        Assert.assertEquals(200, resp.getStatus());
        DeleteResult delResult = resp.readEntity(DeleteResult.class);
        Assert.assertEquals("DelServerR", delResult.getServerName());
        Assert.assertNull(delResult.getErrorDesc());
                
        // Second register notification (after delete)
        WireMock.verify(WireMock.putRequestedFor(WireMock.urlEqualTo("/services"))
                .withRequestBody(WireMock.equalToJson("{\"andonCordServerDetails\":[{\"notificationURL\":\"http://localhost:8104/\",\"serverName\":\"Valid Service\"}]}")));
        
    }

    @Test
    //@Ignore
    @RunAsClient
    public void deleteNonExistingRegisteredServer(@ArquillianResteasyResource("") final WebTarget webTarget) {

        Response resp = webTarget.path("devservice/services").queryParam("serviceName", "DelServerX").queryParam("force", true)
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildDelete().invoke();

        Assert.assertEquals(404, resp.getStatus());
        DeleteResult delResult = resp.readEntity(DeleteResult.class);
        Assert.assertEquals("DelServerX", delResult.getServerName());
        Assert.assertEquals("Service not found to delete [DelServerX]",delResult.getErrorDesc());                        
    }
    
    
    @Test
    //@Ignore
    @RunAsClient
    public void getListOfRegisteredServers(@ArquillianResteasyResource("") final WebTarget webTarget) {

        try {
            System.out.println(webTarget.getUri().toURL().toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        Response resp = webTarget.path("devservice/services")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildGet().invoke();

        Assert.assertEquals(200, resp.getStatus());
        RegisterListVO regListResult = resp.readEntity(RegisterListVO.class);
        Assert.assertNotNull(regListResult);
        Assert.assertNotNull(regListResult.getServersList());
        Assert.assertEquals("Valid Service", regListResult.getServersList().get(0).getServiceName());        
    }
    
    
    @Test
    //@Ignore
    @RunAsClient
    public void updateService(@ArquillianResteasyResource("") final WebTarget webTarget) {
        // Initial End point
        WireMock.stubFor(WireMock.put(WireMock.urlEqualTo("/services"))
                .willReturn(WireMock.aResponse().withStatus(202)
                        .withHeader("Content-Type", "application/json")));   
        
         // Build the Mock for the andoncord notification
        WireMock.stubFor(WireMock.put(WireMock.urlEqualTo("/andoncord"))
                .willReturn(WireMock.aResponse().withStatus(200)
                .withHeader("Content-Type", "application/json")));

        ServiceVO reg = buildValidServiceRegisterVO("Valid Service X1");
        Entity regEntity = Entity.entity(reg, MediaType.APPLICATION_JSON);
        Response resp = webTarget.path("devservice/services")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildPut(regEntity).invoke();

        Assert.assertEquals(202, resp.getStatus());
        RegisterResultVO regResult = resp.readEntity(RegisterResultVO.class);
        Assert.assertEquals(true, regResult.isRegistered());
        Assert.assertNull(regResult.getErrors());

        WireMock.verify(WireMock.putRequestedFor(WireMock.urlEqualTo("/services"))
                .withRequestBody(WireMock.equalToJson("{\"andonCordServerDetails\":[{\"notificationURL\":\"http://localhost:8104/\",\"serverName\":\"Valid Service\"},{\"notificationURL\":\"http://localhost:8104/\",\"serverName\":\"Valid Service X1\"}]}")));
        
        
        // Updated Endpoint
        WireMock.stubFor(WireMock.put(WireMock.urlEqualTo("/updated/services"))
                .willReturn(WireMock.aResponse().withStatus(202)
                        .withHeader("Content-Type", "application/json")));        
        
        reg = buildServiceRegisterVO("Valid Service X1", "shane.bedggood@standardbank.co.za", "http://localhost:8104/updated");
        regEntity = Entity.entity(reg, MediaType.APPLICATION_JSON);
        resp = webTarget.path("devservice/services")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildPut(regEntity).invoke();

        Assert.assertEquals(202, resp.getStatus());
        regResult = resp.readEntity(RegisterResultVO.class);
        Assert.assertEquals(true, regResult.isRegistered());
        Assert.assertNull(regResult.getErrors());

        WireMock.verify(WireMock.putRequestedFor(WireMock.urlEqualTo("/updated/services"))
                .withRequestBody(WireMock.equalTo("{\"andonCordServerDetails\":[{\"notificationURL\":\"http://localhost:8104/\",\"serverName\":\"Valid Service\"},{\"notificationURL\":\"http://localhost:8104/\",\"serverName\":\"Valid Service X1\"}]}")));
        
        // Invoke the Delete of the Updated service
        resp = webTarget.path("devservice/services").queryParam("serviceName", "Valid Service X1").queryParam("force", true)
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildDelete().invoke();
    }
    
    private ServiceVO buildValidServiceRegisterVO(String name) {
        return buildServiceRegisterVO(name, "steven.webber@standardbank.co.za", "http://localhost:8104/", "E", "http://locahost:8104/metrics", "http://localhost:8104/", "http://locahost:8104/");
    }

    private ServiceVO buildServiceRegisterVO(String name, String email) {
        return buildServiceRegisterVO(name, email, "http://localhost:8104/", "E", "http://locahost:8104/metrics", "http://localhost:8104/", "http://locahost:8104/");
    }

    private ServiceVO buildServiceRegisterVO(String name, String email, String registrationURL) {
        return buildServiceRegisterVO(name, email, "http://localhost:8104/", "E", "http://locahost:8104/metrics", registrationURL, "http://locahost:8104/");
    }

    private ServiceVO buildServiceRegisterVO(String name, String email, String andonCord, String location, String metrics, String registration, String healthCheck) {
        ServiceVO regVO = new ServiceVO();
        regVO.setAndonCordStateChangeUrl(andonCord);
        regVO.setEmail(email);
        regVO.setLocation(location);
        regVO.setMetricsUrl(metrics);
        regVO.setRegistrationUrl(registration);
        regVO.setServiceName(name);
        regVO.setHealthCheckUrl(healthCheck);
        return regVO;
    }

}
