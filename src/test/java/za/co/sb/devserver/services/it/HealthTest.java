/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.sb.devserver.services.it;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.GreenMailUtil;
import com.icegreen.greenmail.util.ServerSetupTest;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.extension.rest.client.ArquillianResteasyResource;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.FileAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.*;
import org.junit.runner.RunWith;
import za.co.sb.devserver.entity.Register;
import za.co.sb.devserver.rest.vo.RegisterResultVO;
import za.co.sb.devserver.rest.vo.ServiceVO;

import javax.mail.MessagingException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 *
 * @author steven
 */
@RunWith(Arquillian.class)
public class HealthTest {

    String baseURL = "http://localhost:8104/";

    @ArquillianResource
    private URL deploymentURL;

    GreenMail greenMail;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options().port(8104));

    public HealthTest() {
    }

    @Deployment
    public static Archive<?> deployment() {
        WebArchive jar = ShrinkWrap.create(WebArchive.class, "test.war")
                .addClass(Register.class)
                .addPackage("za.co.sb.devserver.clients.devops")
                .addPackage("za.co.sb.devserver.entity")
                .addPackage("za.co.sb.devserver.clients.feature")
                .addPackage("za.co.sb.devserver.services")                
                .addClass("za.co.sb.devserver.rest.DevServiceApplication")
                .addClass("za.co.sb.devserver.rest.ServiceResource")
                .addClass("za.co.sb.devserver.rest.BeanViolationExceptionMapper")
                .addClass("za.co.sb.devserver.rest.HealthResource")
                .addClass("za.co.sb.devserver.services.BuildService")
                .addPackage("za.co.sb.devserver.health")
                .addPackage("za.co.sb.devserver.rest.vo")
                .addAsResource(new FileAsset(new File("src/test/resources/META-INF/persistence.xml")), "META-INF/persistence.xml")
                .addAsResource(new FileAsset(new File("src/test/resources/META-INF/microprofile-config.properties")), "META-INF/microprofile-config.properties")
                .addAsWebInfResource(new FileAsset(new File("src/main/webapp/WEB-INF/web.xml")), "web.xml");
        return jar;

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        greenMail = new GreenMail(ServerSetupTest.SMTP);
        greenMail.start();
    }

    @After
    public void tearDown() {
        greenMail.stop();
    }

    @Test
    @RunAsClient
    public void doHealthChecksWorking(@ArquillianResteasyResource("") final WebTarget webTarget) {

        // Build the Mock
        WireMock.stubFor(WireMock.put(WireMock.urlEqualTo("/services"))
                .willReturn(WireMock.aResponse().withStatus(202)
                        .withHeader("Content-Type", "application/json")));
        
         // Build the Mock for the andoncord notification
        WireMock.stubFor(WireMock.put(WireMock.urlEqualTo("/andoncord"))
                .willReturn(WireMock.aResponse().withStatus(200)
                .withHeader("Content-Type", "application/json")));

        ServiceVO ser = getServiceVO("Service1", "A");
        Entity regEntity = Entity.entity(ser, MediaType.APPLICATION_JSON);
        Response resp = webTarget.path("devservice/services")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildPut(regEntity).invoke();

        Assert.assertEquals(202, resp.getStatus());
        RegisterResultVO regResult = resp.readEntity(RegisterResultVO.class);
        Assert.assertEquals(true, regResult.isRegistered());
        Assert.assertNull(regResult.getErrors());

        ser = getServiceVO("Service2", "B");
        regEntity = Entity.entity(ser, MediaType.APPLICATION_JSON);
        resp = webTarget.path("devservice/services")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildPut(regEntity).invoke();

        Assert.assertEquals(202, resp.getStatus());
        regResult = resp.readEntity(RegisterResultVO.class);
        Assert.assertEquals(true, regResult.isRegistered());
        Assert.assertNull(regResult.getErrors());

        WireMock.stubFor(WireMock.get(WireMock.urlEqualTo("/health"))
                .willReturn(WireMock.aResponse().withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"outcome\":\"UP\",\"checks\":[{\"name\":\"memory-check\",\"state\":\"UP\",\"data\":{\"Free Memory\":\"728402\",\"Percentage Free\":\"68.6944052541043%\",\"Max Memory\":\"1060352\"}}]}")));

        // Force health check
        resp = webTarget.path("devservice/health")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildGet().invoke();

        WireMock.verify(2, WireMock.getRequestedFor(WireMock.urlEqualTo("/health")));
        Assert.assertEquals(200, resp.getStatus());
    }

    @Test
    @RunAsClient
    public void doHealthCheckDown(@ArquillianResteasyResource("") final WebTarget webTarget) {

        // Build the Mock
        WireMock.stubFor(WireMock.put(WireMock.urlEqualTo("/services"))
                .willReturn(WireMock.aResponse().withStatus(202)
                        .withHeader("Content-Type", "application/json")));

         // Build the Mock for the andoncord notification
        WireMock.stubFor(WireMock.put(WireMock.urlEqualTo("/andoncord"))
                .willReturn(WireMock.aResponse().withStatus(200)
                .withHeader("Content-Type", "application/json")));

        ServiceVO ser = getServiceVO("Service1", "A");
        Entity regEntity = Entity.entity(ser, MediaType.APPLICATION_JSON);
        Response resp = webTarget.path("devservice/services")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildPut(regEntity).invoke();

        Assert.assertEquals(202, resp.getStatus());
        RegisterResultVO regResult = resp.readEntity(RegisterResultVO.class);
        Assert.assertEquals(true, regResult.isRegistered());
        Assert.assertNull(regResult.getErrors());

        ser = getServiceVO("Service2", "B");
        regEntity = Entity.entity(ser, MediaType.APPLICATION_JSON);
        resp = webTarget.path("devservice/services")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildPut(regEntity).invoke();

        Assert.assertEquals(202, resp.getStatus());
        regResult = resp.readEntity(RegisterResultVO.class);
        Assert.assertEquals(true, regResult.isRegistered());
        Assert.assertNull(regResult.getErrors());

        WireMock.stubFor(WireMock.get(WireMock.urlEqualTo("/health"))
                .willReturn(WireMock.aResponse().withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"outcome\":\"DOWN\",\"checks\":[{\"name\":\"memory-check\",\"state\":\"DOWN\",\"data\":{\"Free Memory\":\"728402\",\"Percentage Free\":\"68.6944052541043%\",\"Max Memory\":\"1060352\"}}]}")));

        // Force health check
        resp = webTarget.path("devservice/health")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildGet().invoke();

        WireMock.verify(2, WireMock.getRequestedFor(WireMock.urlEqualTo("/health")));
        Assert.assertEquals(200, resp.getStatus());

        // Confirm email was sent
        try {
            String subject = greenMail.getReceivedMessages()[0].getSubject();
            assertEquals("DevService Notification - Health check - down", subject);
            String text = GreenMailUtil.getBody(greenMail.getReceivedMessages()[0]);
            System.out.println(text);
                        
//            assertEquals("Oh dear Service1\n"
//                    + "\n"
//                    + "Your service isn't doing all that well.\n"
//                    + "When I asked, your health check returned a DOWN result.\n"
//                    + "The details that you supplied to me are below.\n"
//                    + "\n"
//                    + "memory-check : DOWN\n"
//                    + "   Free Memory 728402\n"
//                    + "   Percentage Free 68.6944052541043%\n"
//                    + "   Max Memory 1060352\n"
//                    + "\n\n"
//                    + "Regards,\n"
//                    + "DevService", text);            

        } catch (MessagingException mEx) {
            fail(mEx.getMessage());
        }
    }

    @Test
    @RunAsClient
    public void doHealthUnexpectedStatus(@ArquillianResteasyResource("") final WebTarget webTarget) {

        // Build the Mock
        WireMock.stubFor(WireMock.put(WireMock.urlEqualTo("/services"))
                .willReturn(WireMock.aResponse().withStatus(202)
                        .withHeader("Content-Type", "application/json")));
        
         // Build the Mock for the andoncord notification
        WireMock.stubFor(WireMock.put(WireMock.urlEqualTo("/andoncord"))
                .willReturn(WireMock.aResponse().withStatus(200)
                .withHeader("Content-Type", "application/json")));

        ServiceVO ser = getServiceVO("Service1", "A");
        Entity regEntity = Entity.entity(ser, MediaType.APPLICATION_JSON);
        Response resp = webTarget.path("devservice/services")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildPut(regEntity).invoke();

        Assert.assertEquals(202, resp.getStatus());
        RegisterResultVO regResult = resp.readEntity(RegisterResultVO.class);
        Assert.assertEquals(true, regResult.isRegistered());
        Assert.assertNull(regResult.getErrors());

        ser = getServiceVO("Service2", "B");
        regEntity = Entity.entity(ser, MediaType.APPLICATION_JSON);
        resp = webTarget.path("devservice/services")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildPut(regEntity).invoke();

        Assert.assertEquals(202, resp.getStatus());
        regResult = resp.readEntity(RegisterResultVO.class);
        Assert.assertEquals(true, regResult.isRegistered());
        Assert.assertNull(regResult.getErrors());

        WireMock.stubFor(WireMock.get(WireMock.urlEqualTo("/health"))
                .willReturn(WireMock.aResponse().withStatus(202)
                        .withHeader("Content-Type", "application/json")));

        // Force health check
        resp = webTarget.path("devservice/health")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildGet().invoke();

        WireMock.verify(2, WireMock.getRequestedFor(WireMock.urlEqualTo("/health")));
        
        Assert.assertEquals(200, resp.getStatus());

        // Confirm email was sent
        try {
            String subject = greenMail.getReceivedMessages()[0].getSubject();
            assertEquals("DevService Notification - Health check - status", subject);
            String text = GreenMailUtil.getBody(greenMail.getReceivedMessages()[0]);
//            assertEquals("Oh dear Service1\n"
//                    + "\n"
//                    + "Your service isn't responding to my health checks.\n"
//                    + "When I asked, your service responded with a status code of 202\n"                    
//                    + "\n"
//                    + "\n"
//                    + "Regards,\n"
//                    + "DevService", text);

        } catch (MessagingException mEx) {
            fail(mEx.getMessage());
        }
    }
    
    
    @Test
    @RunAsClient
    public void doHealthCheckException(@ArquillianResteasyResource("") final WebTarget webTarget) {

        // Build the Mock
        WireMock.stubFor(WireMock.put(WireMock.urlEqualTo("/services"))
                .willReturn(WireMock.aResponse().withStatus(202)
                        .withHeader("Content-Type", "application/json")));

         // Build the Mock for the andoncord notification
        WireMock.stubFor(WireMock.put(WireMock.urlEqualTo("/andoncord"))
                .willReturn(WireMock.aResponse().withStatus(200)
                .withHeader("Content-Type", "application/json")));

        ServiceVO ser = getServiceVO("Service1", "A");
        Entity regEntity = Entity.entity(ser, MediaType.APPLICATION_JSON);
        Response resp = webTarget.path("devservice/services")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildPut(regEntity).invoke();

        Assert.assertEquals(202, resp.getStatus());
        RegisterResultVO regResult = resp.readEntity(RegisterResultVO.class);
        Assert.assertEquals(true, regResult.isRegistered());
        Assert.assertNull(regResult.getErrors());

        ser = getServiceVO("Service2", "B");
        regEntity = Entity.entity(ser, MediaType.APPLICATION_JSON);
        resp = webTarget.path("devservice/services")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildPut(regEntity).invoke();

        Assert.assertEquals(202, resp.getStatus());
        regResult = resp.readEntity(RegisterResultVO.class);
        Assert.assertEquals(true, regResult.isRegistered());
        Assert.assertNull(regResult.getErrors());

        WireMock.stubFor(WireMock.get(WireMock.urlEqualTo("/health"))
                .willReturn(WireMock.aResponse().withStatus(404)
                        .withHeader("Content-Type", "application/json")));

        // Force health check
        resp = webTarget.path("devservice/health")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .buildGet().invoke();

        WireMock.verify(2, WireMock.getRequestedFor(WireMock.urlEqualTo("/health")));
        
        Assert.assertEquals(200, resp.getStatus());

        // Confirm email was sent
        try {
            String subject = greenMail.getReceivedMessages()[0].getSubject();
            assertEquals("DevService Notification - Health check - exception", subject);
            String text = GreenMailUtil.getBody(greenMail.getReceivedMessages()[0]);
//            assertEquals("Oh dear Service1\n"
//                    + "\n"
//                    + "Your service isn't responding to my health checks.\n"
//                    + "When I asked, your service caused an exception with message HTTP 404 Not Found\n"
//                    + "\n"
//                    + "\n"
//                    + "Regards,\n"
//                    + "DevService", text);

        } catch (MessagingException mEx) {
            fail(mEx.getMessage());
        }
    }
    
    private ServiceVO getServiceVO(String name, String location) {
        ServiceVO ser = new ServiceVO();
        ser.setAndonCordStateChangeUrl(baseURL);
        ser.setEmail("steven.webber@standardbank.co.za");
        ser.setHealthCheckUrl(baseURL);
        ser.setLocation(location);
        ser.setMetricsUrl(baseURL);
        ser.setRegistrationUrl(baseURL);
        ser.setServiceName(name);
        return ser;
    }
}
