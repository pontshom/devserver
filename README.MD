# DevServer

## The Andon Cord
DevServer performs functions related to the Andon Cord. The Andon Cord is inactive or active depending on the
status of a projects build/s. It's used to indicate that the CI/CD pipeline is able to produce valid artifacts 
or that the pipeline is broken. That is, that the items moving through the pipeline
are not working. This is determined by examining the status of a certain group of builds
if any one of those builds has failed, the Andon Cord is pulled/raised. Is stays up until the build is corrected.

When the Andon Cord is up, developers can't commit code. Only the "fix" can be committed.


## DevServer
DevServer does not itself determine if the Andon Cord should be up or down. It simply 
asks the Andon Cord service via a REST call. What the DevServer does provide however 
is the following 

### Andon Cord
* Provides a REST end point that reports on the status of the cord.
* Force the a change to the status of the Cord.

### Notifications
* Allow you to register your interest in the status of the cord for notifications
* Obtain a list of all servers registered for notification
* Delete a registered server
* Keep your server informed of all changes to the list of registered servers.
* Notify you via email of issues with your registered server


## Useful commands and URL's
* Starting the Server - mvn payara-micro:start
* openapi-ui localhost:8080/devservice/openapi-ui



## ToDo

Fork the repository.
Make changes in the forked copy.
Raise a pull request against the develop branch with the following new feature: 
 
Given that the DevServer is aware of when the cord is up or down, create a REST 
service that will return stats indicating 

* what percentage of a provided period it's been up/pulled E.g. 50% of the last 24 hours

* which builds/projects where responsible during that period

* ordered list of which builds/projects where the most responsible (Number of times 
they pulled the cord). E.g. Payments pulled the cord twice

* list of projects and time, ordered by how long the cord was up/time to repair 
for each project


